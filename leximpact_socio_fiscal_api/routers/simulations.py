import asyncio
from datetime import date
from importlib import metadata
import json
import math
from typing import Any

from fastapi import APIRouter, Depends, HTTPException, Request, WebSocket
from pydantic import BaseModel, Field

# import numpy as np
from openfisca_core.errors import SituationParsingError
from openfisca_core.holders import set_input_divide_by_period
from openfisca_core.parameters import helpers, ParameterNode, ParameterScaleBracket
from openfisca_core.periods import (
    DAY,
    ETERNITY,
    instant,
    MONTH,
    period as new_period,
    YEAR,
)
from openfisca_core.reforms import Reform
from openfisca_core.scripts import build_tax_benefit_system
from openfisca_core.simulation_builder import (
    calculate_output_add,
    calculate_output_divide,
    SimulationBuilder,
)
from openfisca_core.taxbenefitsystems import TaxBenefitSystem

from .. import config


class CalculateParameters(BaseModel):
    parametric_reform: dict[str, Any] | None = Field(
        default=None, description="Parametric reform to apply"
    )
    period: str = Field(description="Date to use for simulation")
    reform: str | None = Field(default=None, description="Name of reform")
    situation: dict[str, Any] = Field(
        description="The OpenFisca situation of test cases"
    )
    title: str = Field(description="Human readable title of simulation")
    token: str | None = Field(
        default=None,
        description="Unique value used to identify simulation in logs and response",
    )
    trace: bool = Field(default=False, description="Add trace steps to response")
    variables: list[str] = Field(
        description="Names of OpenFisca variables to calculate"
    )


class Evaluation(BaseModel):
    entity: str = Field(
        description="The name of the entity that the variable belongs to"
    )
    name: str = Field(description="Variable name")
    value: list[bool] | list[float] | list[int] | list[str] = (
        "Vector of calculated value of variable"
    )


class EvaluationStep(BaseModel):
    name: str = Field(description="Variable name")
    period: str = Field(description="period at which the variable was evaluated")
    value: list[bool] | list[float] | list[int] | list[str] = (
        "Vector of calculated value of variable"
    )


class CalculateResult(BaseModel):
    evaluations: list[Evaluation]
    token: str | None = Field(
        default=None,
        description="Token that was given in request",
    )
    trace: list[EvaluationStep] | None = Field(default=None, description="")


settings = config.get_settings()
reform_class_by_name = {
    reform_name: reform_class
    for reform_name, reform_class in (
        (reform_entry_point.name, reform_entry_point.load())
        for reform_entry_point in metadata.entry_points().get("openfisca.reforms", [])
    )
    if reform_class.tax_benefit_system_name == settings.country_package
}
print("Available reforms:", ", ".join(reform_class_by_name.keys()))
router = APIRouter(
    prefix="/simulations",
    tags=["simulations"],
)
tax_benefit_system_by_name = None


# Caution: This function modify existing parameters instead of duplicating them.
def apply_parametric_reform(parameters, parameter_change_by_name):
    errors = {}
    for name, change in parameter_change_by_name.items():
        ids = name.split(".")
        parameter = parameters
        for id in ids:
            # Note: Sometimes, after a preprocessing, getattr(parameter, id, None)
            # doesn't work => Use children instead.
            children = getattr(parameter, "children", None)
            if children is None:
                errors[name] = (
                    f"Parameter {id} doesn't exist, because parent has no children"
                )
                break
            parameter = children.get(id)
            if parameter is None:
                errors[name] = f"Parameter doesn't exist. Missing {id}"
                break
        else:
            changeType = change.get("type")
            if changeType == "parameter":
                parameter.update(
                    start=instant(change["start"]) if change.get("start") != None else None,
                    stop=instant(change["stop"]) if change.get("stop") != None else None,
                    value=change.get("value"),
                )
            elif changeType == "scale":
                # TODO: handle stop?.
                if change.get("stop") is not None:
                    errors[name] = "Scale change can't contain a 'stop'"
                    break
                # Note: change has the form:
                # {
                #     'scale': [
                #         {'rate': {'value': 0.5}, 'threshold': {'value': 0}},
                #         {'rate': {'value': 0}, 'threshold': {'value': 4}},
                #     ],
                #     'start': '2021-01-01',
                #     'type': 'scale',
                # }
                # This is not the same structure as OpenFisca brackets
                # => Convert it.
                brackets = parameter.brackets
                value_key = (
                    "amount"
                    if any("amount" in bracket.children for bracket in brackets)
                    else (
                        "average_rate"
                        if any(
                            "average_rate" in bracket.children for bracket in brackets
                        )
                        else "rate"
                    )
                )

                brackets_change = []
                for bracket in change["scale"]:
                    bracket_change = {}

                    threshold = bracket["threshold"]
                    if threshold == "expected":
                        errors[name] = (
                            "Brackets with 'expected' values are not supported."
                        )
                        break
                    value = threshold["value"]
                    if value is None:
                        # Ignore brackets with a null threshold.
                        continue
                    bracket_change["threshold"] = value

                    if value_key == "amount":
                        amount = bracket["amount"]
                        if amount == "expected":
                            errors[name] = (
                                "Brackets with 'expected' values are not supported."
                            )
                            break
                        bracket_change["amount"] = amount["value"]
                    elif value_key == "average_rate":
                        rate = bracket["rate"]
                        if rate == "expected":
                            errors[name] = (
                                "Brackets with 'expected' values are not supported."
                            )
                            break
                        bracket_change["average_rate"] = rate["value"]
                    else:
                        base = bracket.get("base")
                        if base is not None:
                            if base == "expected":
                                errors[name] = (
                                    "Brackets with 'expected' values are not supported."
                                )
                                break
                            bracket_change["base"] = base["value"]
                        rate = bracket["rate"]
                        if rate == "expected":
                            errors[name] = (
                                "Brackets with 'expected' values are not supported."
                            )
                            break
                        bracket_change["rate"] = rate["value"]
                    brackets_change.append(bracket_change)
                else:
                    # Brackets change contains no error.
                    brackets_change.sort(
                        key=lambda bracket_change: bracket_change["threshold"]
                    )
                    start = change["start"]
                    for index, bracket_change in enumerate(brackets_change):
                        if len(brackets) <= index:
                            brackets.append(
                                ParameterScaleBracket(
                                    name=helpers._compose_name(
                                        parameter.name, item_name=index
                                    ),
                                    data={
                                        key: {start: value_change}
                                        for key, value_change in bracket_change.items()
                                    },
                                )
                            )
                        else:
                            bracket_dict = brackets[index].children
                            for key, value_change in bracket_change.items():
                                value = bracket_dict.get(key)
                                if value is None:
                                    bracket_dict[key] = dict(
                                        start=instant(start),
                                        value=value_change,
                                    )
                                else:
                                    value.update(
                                        start=instant(start),
                                        value=value_change,
                                    )
                    if len(brackets) > len(brackets_change):
                        del brackets[len(brackets_change) :]
            else:
                errors[name] = f"Change type {changeType} doesn't exist."
    return errors or None


def get_tax_benefit_system_by_name(
    settings: config.Settings = Depends(config.get_settings),
):
    global tax_benefit_system_by_name
    if tax_benefit_system_by_name is None:
        tax_benefit_system = build_tax_benefit_system(
            settings.country_package,
            None,  # settings.extension,
            None,  # settings.reform,
        )
        tax_benefit_system_by_name = {
            reform_name: reform_class(tax_benefit_system)
            for reform_name, reform_class in reform_class_by_name.items()
        }
        tax_benefit_system_by_name[""] = tax_benefit_system
    return tax_benefit_system_by_name


def iter_trace_steps(tracer):
    for step in tracer.trees:
        yield from iter_trace_steps1(step)


def iter_trace_steps1(step):
    for child in step.children:
        yield from iter_trace_steps1(child)
    yield step


@router.post("")
async def calculate(
    request: Request,
    parameters: CalculateParameters,
    settings: config.Settings = Depends(config.get_settings),
    tax_benefit_system_by_name: TaxBenefitSystem = Depends(
        get_tax_benefit_system_by_name
    ),
) -> CalculateResult:
    reform_name = parameters.reform
    if reform_name is None:
        tax_benefit_system = tax_benefit_system_by_name[""]
    else:
        reform = tax_benefit_system_by_name.get(reform_name)
        if reform is None:
            raise HTTPException(status_code=400, detail="Reform not found")
        tax_benefit_system = reform

    parametric_reform = (
        None
        if parameters.parametric_reform is None
        else {
            parameter_name: parameter_value
            for parameter_name, parameter_value in parameters.parametric_reform.items()
            if parameter_value is not None
        }
        or None
    )
    if parametric_reform is not None:

        def simulation_modifier(parameters: ParameterNode):
            parametric_reform_errors = apply_parametric_reform(
                parameters, parametric_reform
            )
            if parametric_reform_errors is not None:
                errors_json = json.dumps(
                    parametric_reform_errors, ensure_ascii=False, indent=2
                )
                raise HTTPException(
                    status_code=400,
                    detail=f"Error(s) when applying reform: {errors_json}",
                )
            return parameters

        class SimulationReform(Reform):
            def apply(self):
                self.modify_parameters(modifier_function=simulation_modifier)

        tax_benefit_system = SimulationReform(tax_benefit_system)

    simulation_builder = SimulationBuilder()
    try:
        simulation = simulation_builder.build_from_entities(
            tax_benefit_system, parameters.situation
        )
    except SituationParsingError as e:
        errors_json = json.dumps(e.error, ensure_ascii=False, indent=2)
        raise HTTPException(
            status_code=400,
            detail=f"Error(s) when building simulation from situation: {errors_json}",
        )
    if parameters.trace:
        simulation.trace = True

    debug = settings.debug
    evaluations = []
    period = new_period(parameters.period)
    title = parameters.title
    token = parameters.token
    trace = parameters.trace
    for variable_name in parameters.variables:
        if await request.is_disconnected():
            print("Request disconnected. Stopping calculations")
            raise HTTPException(503, "Request disconnected!")
        print("Calculating variable", variable_name)
        # value = simulation.calculate_add(variable_name, period)
        variable = simulation.tax_benefit_system.get_variable(variable_name)
        if variable is None:
            short_token = token.split("-")[0] if token else ""
            full_title = title
            if reform_name is not None:
                full_title += "+" + reform_name
            raise HTTPException(
                status_code=400,
                detail="Requested variable doesn't exist:"
                + f"{full_title}@{short_token}:{variable_name}",
            )
        value = (
            simulation.calculate(variable_name, period)
            if variable.definition_period is ETERNITY
            or variable.definition_period is YEAR
            else (
                simulation.calculate_add(variable_name, period)
                if variable.calculate_output is calculate_output_add
                or variable.calculate_output is calculate_output_divide
                or variable.set_input is set_input_divide_by_period
                else (
                    simulation.calculate(variable_name, period.first_day)
                    if variable.definition_period is DAY
                    else (
                        simulation.calculate(variable_name, period.first_month)
                        if variable.definition_period is MONTH
                        else simulation.calculate(variable_name, period)
                    )
                )
            )
        )
        population = simulation.get_variable_population(variable_name)
        entity = population.entity
        # entity_count = simulation_builder.entity_counts[entity.plural]
        # sum = (
        #     np.sum(
        #         np.split(
        #             value,
        #             population.count // entity_count,
        #         ),
        #         1,
        #     )
        #     if entity_count > 1
        #     else value
        # )
        if debug:
            short_token = token.split("-")[0] if token else ""
            full_title = title
            if reform_name is not None:
                full_title += "+" + reform_name
            if parametric_reform is not None:
                full_title += "+parametric_reform"
            print(
                f"Calculated {full_title}@{short_token}:{variable_name}",
                f"({entity.key}): {value}",
            )
        evaluations.append(
            dict(
                entity=entity.key,
                name=variable_name,
                value=[
                    (
                        item.decode()
                        if isinstance(item, bytes)
                        else (
                            item.isoformat()
                            if isinstance(item, date)
                            else (
                                0
                                if isinstance(item, float) and math.isnan(item)
                                else item
                            )
                        )
                    )
                    for item in value.tolist()
                ],
            )
        )
    result = CalculateResult(evaluations=evaluations)

    if token is not None:
        result.token = token

    if trace:
        encountered = set()
        steps = []
        for step in iter_trace_steps(simulation.tracer):
            current = (step.name, str(step.period))
            if current not in encountered:
                encountered.add(current)
                steps.append(
                    dict(
                        name=step.name,
                        period=str(step.period),
                        value=[
                            (
                                item.decode()
                                if isinstance(item, bytes)
                                else (
                                    item.isoformat()
                                    if isinstance(item, date)
                                    else (
                                        0
                                        if isinstance(item, float) and math.isnan(item)
                                        else item
                                    )
                                )
                            )
                            for item in step.value.tolist()
                        ],
                    )
                )
        result.trace = steps

    return result


@router.websocket("/calculate")
async def calculate_webocket(
    websocket: WebSocket,
    settings: config.Settings = Depends(config.get_settings),
    tax_benefit_system_by_name: TaxBenefitSystem = Depends(
        get_tax_benefit_system_by_name
    ),
):
    await websocket.accept()
    debug = settings.debug
    parametric_reform = None
    period = None
    reform_name = None
    reform_tax_benefit_system = tax_benefit_system_by_name[""]
    simulation_tax_benefit_system = tax_benefit_system_by_name[""]
    situation = None
    title = None
    token = None
    variables_name = None
    while True:
        data = await websocket.receive_json()
        calculate = False
        errors = {}
        trace = False

        if "reform_name" in data:
            reform_name = data["reform_name"]
            if debug:
                print("Received reform_name:", reform_name)
            if reform_name is None:
                reform_tax_benefit_system = tax_benefit_system_by_name[""]
            else:
                new_reform_tax_benefit_system = tax_benefit_system_by_name.get(
                    reform_name
                )
                if new_reform_tax_benefit_system is None:
                    errors["reform_name"] = "Unknown tax benefit system"
                    reform_tax_benefit_system = tax_benefit_system_by_name[""]
                else:
                    reform_tax_benefit_system = new_reform_tax_benefit_system
            simulation_tax_benefit_system = reform_tax_benefit_system
        for key, value in data.items():
            if key == "calculate":
                calculate = True
            if key == "parametric_reform":
                if debug:
                    print("Received parametric_reform.")
                parametric_reform = (
                    None
                    if value is None
                    else {
                        parameter_name: parameter_value
                        for parameter_name, parameter_value in value.items()
                        if parameter_value is not None
                    }
                    or None
                )
                if parametric_reform:

                    def simulation_modifier(parameters: ParameterNode):
                        parametric_reform_errors = apply_parametric_reform(
                            parameters, parametric_reform
                        )
                        if parametric_reform_errors is not None:
                            errors["parametric_reform"] = parametric_reform_errors
                        return parameters

                    class SimulationReform(Reform):
                        def apply(self):
                            self.modify_parameters(
                                modifier_function=simulation_modifier
                            )

                    simulation_tax_benefit_system = SimulationReform(
                        reform_tax_benefit_system
                    )
                else:
                    simulation_tax_benefit_system = reform_tax_benefit_system
                continue
            if key == "period":
                if debug:
                    print("Received period.")
                period = new_period(value)
                continue
            if key == "reform_name":
                # Already handled above.
                continue
            if key == "situation":
                if debug:
                    print("Received situation.")
                situation = value
                continue
            if key == "title":
                if debug:
                    print("Received title.")
                title = value
                continue
            if key == "token":
                if debug:
                    print("Received token.")
                token = value
                continue
            if key == "trace":
                if debug:
                    print("Received trace.")
                trace = True
                continue
            if key == "variables":
                if debug:
                    print("Received names of variables to calculate.")
                variables_name = value
                continue

        if errors:
            print("Error:", errors)
            await websocket.send_json(dict(errors=errors))
            continue

        if calculate:
            if debug:
                print("Calculating…")

            if not variables_name:
                errors["variables"] = "Missing value"
            if period is None:
                errors["period"] = "Missing value"
            if not situation:
                errors["situation"] = "Missing value"
            simulation_builder = SimulationBuilder()

            if errors:
                print("Error:", errors)
                await websocket.send_json(dict(errors=errors))
                continue

            try:
                simulation = simulation_builder.build_from_entities(
                    simulation_tax_benefit_system, situation
                )
            except SituationParsingError as e:
                errors["build_from_entities"] = e.error
                print("Error:", errors)
                await websocket.send_json(dict(errors=errors))
                continue
            if trace:
                simulation.trace = True

            for variable_name in variables_name:
                print("Calculating variable", variable_name)
                # value = simulation.calculate_add(variable_name, period)
                variable = simulation.tax_benefit_system.get_variable(variable_name)
                if variable is None:
                    short_token = token.split("-")[0] if token else ""
                    full_title = title
                    if reform_name is not None:
                        full_title += "+" + reform_name
                    print(
                        "Requested variable doesn't exist:",
                        f"{full_title}@{short_token}:{variable_name}",
                    )
                    continue
                value = (
                    simulation.calculate(variable_name, period)
                    if variable.definition_period is ETERNITY
                    or variable.definition_period is YEAR
                    else (
                        simulation.calculate_add(variable_name, period)
                        if variable.calculate_output is calculate_output_add
                        or variable.calculate_output is calculate_output_divide
                        or variable.set_input is set_input_divide_by_period
                        else (
                            simulation.calculate(variable_name, period.first_day)
                            if variable.definition_period is DAY
                            else (
                                simulation.calculate(variable_name, period.first_month)
                                if variable.definition_period is MONTH
                                else simulation.calculate(variable_name, period)
                            )
                        )
                    )
                )
                population = simulation.get_variable_population(variable_name)
                entity = population.entity
                # entity_count = simulation_builder.entity_counts[entity.plural]
                # sum = (
                #     np.sum(
                #         np.split(
                #             value,
                #             population.count // entity_count,
                #         ),
                #         1,
                #     )
                #     if entity_count > 1
                #     else value
                # )
                if debug:
                    short_token = token.split("-")[0] if token else ""
                    full_title = title
                    if reform_name is not None:
                        full_title += "+" + reform_name
                    if parametric_reform is not None:
                        full_title += "+parametric_reform"
                    print(
                        f"Calculated {full_title}@{short_token}:{variable_name}",
                        f"({entity.key}): {value}",
                    )
                await websocket.send_json(
                    dict(
                        entity=entity.key,
                        name=variable_name,
                        token=token,
                        value=[
                            (
                                item.decode()
                                if isinstance(item, bytes)
                                else (
                                    item.isoformat()
                                    if isinstance(item, date)
                                    else (
                                        0
                                        if isinstance(item, float) and math.isnan(item)
                                        else item
                                    )
                                )
                            )
                            for item in value.tolist()
                        ],
                    )
                )
                await asyncio.sleep(0)
            if trace:
                encountered = set()
                steps = []
                for step in iter_trace_steps(simulation.tracer):
                    current = (step.name, str(step.period))
                    if current not in encountered:
                        encountered.add(current)
                        steps.append(
                            dict(
                                name=step.name,
                                period=str(step.period),
                                value=[
                                    (
                                        item.decode()
                                        if isinstance(item, bytes)
                                        else (
                                            item.isoformat()
                                            if isinstance(item, date)
                                            else (
                                                0
                                                if isinstance(item, float)
                                                and math.isnan(item)
                                                else item
                                            )
                                        )
                                    )
                                    for item in step.value.tolist()
                                ],
                            )
                        )
                await websocket.send_json(
                    dict(
                        trace=steps,
                        token=token,
                    )
                )
                await asyncio.sleep(0)
            await websocket.send_json(
                dict(
                    done=True,
                    token=token,
                )
            )
            await asyncio.sleep(0)
