from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .routers import simulations


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,  # Can't be True when allow_origins is set to ["*"].
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(simulations.router)
