# LexImpact Socio-Fiscal API

_HTTP + WebSocket API for OpenFisca_

Used by [LexImpact Socio-Fiscal UI](https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-ui), a simulator of the French tax-benefit system.

## Installation

```bash
git clone https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-api.git
cd leximpact-socio-fiscal-api/
poetry install

poetry shell
# Create environment configuration file and then edit it to reference country package et its JSON output.
cp example.env .env
```

## Usage

Start web API server:

```bash
poetry shell
uvicorn --reload leximpact_socio_fiscal_api.main:app
```
